package repository;

import model.Book;
import model.validators.IValidator;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Validator;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


public class BookFileRepository extends InMemoryRepository<Integer, Book> {
    private String fileName;

    public BookFileRepository(IValidator<Book> validator, String fileName) {
        super(validator);
        this.fileName = fileName;

        loadData();
//        loadDataWithJavaNio();
    }

    private void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                int id = Integer.parseInt(items.get(0));
                String name = items.get(1);
                String author = items.get((2));
                String publisher = items.get((3));
                String yearOfPublication = items.get((4));
                double price =Double.parseDouble(items.get(5));

                Book book = new Book(id, name, author, publisher, yearOfPublication, price);
                book.setId(id);

                try {
                    super.save(book);
                } catch (ValidatorException | IOException | SAXException | ParserConfigurationException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Load data from file with java.nio - the entirely data is read from one call
     * For our example is not useful
     */
    private void loadDataWithJavaNio() {
        try
        {
            RandomAccessFile aFile = new RandomAccessFile("./data/books.txt","r");
            FileChannel inChannel = aFile.getChannel();
            long fileSize = inChannel.size();
            ByteBuffer buffer = ByteBuffer.allocate((int) fileSize);
            inChannel.read(buffer);
            //buffer.rewind();
            buffer.flip();
            //System.out.print(fileSize);
            for (int i = 0; i < fileSize; i++) {
                System.out.print((char) buffer.get());
            }
            inChannel.close();
            aFile.close();
        }
        catch (IOException exc)
        {
            System.out.println(exc);
            System.exit(1);
        }
    }

    @Override
    public Optional<Book> save(Book entity) throws ValidatorException, ParserConfigurationException, SAXException, IOException {
        Optional<Book> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    private void saveToFile(Book entity) {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getName() + "," + entity.getAuthor() + "," + entity.getPublisher() + "," + entity.getYearOfPublication() + "," + entity.getPrice());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}


