package repository;

import model.Book;
import model.Entity;
import model.validators.IValidator;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class InMemoryRepository<ID, T extends Entity<ID>> implements IRepository<ID, T> {
    private Map<ID, T> entities;
    private IValidator<T> validator;


    /**
     * Creates a repository for a specific type implementation object.
     */

    public InMemoryRepository(IValidator<T> validator) {
        this.validator = validator;
        entities = new HashMap<>();
    }

    /**
     * Find the Entity with the given {@code id}.
     *
     * @param id must be not null.
     * @return a {@code Entity} with the given id.
     * @throws IllegalArgumentException if the given id is null.
     */
    @Override
    public Optional<T> findOne(ID id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        return Optional.ofNullable(entities.get(id));
    }

    /**
     * Returns all entities from repository.
     *
     * @return a set of {@code Entities}.
     */
    @Override
    public Iterable<T> findAll() {
        Set<T> allEntities = entities.entrySet().stream().map( Map.Entry::getValue).collect( Collectors.toSet());
        return allEntities;
    }

    /**
     * Saves the given Entity.
     *
     * @param entity must not be null.
     * @return a {@code Book} - null if the book was saved, otherwise (e.g. id already exists) returns the entity.
     * @throws ValidatorException if the given book is null.
     */
    @Override
    public Optional save(T entity) throws ValidatorException, IOException, SAXException, ParserConfigurationException {
        if (entity == null) {
            throw new ValidatorException("id must not be null");
        }
        validator.validate(entity);
        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    /**
     * Removes the Entity with the given id.
     *
     * @param id must not be null.
     * @return a {@code Entity} - null if there is no entity with the given id, otherwise the removed entity.
     * @throws IllegalArgumentException if the given id is null.
     */
    @Override
    public Optional<T> delete(ID id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        return Optional.ofNullable(entities.remove(id));
    }

    /**
     * Updates the given Book.
     *
     * @param entity must not be null.
     * @return a {@code Book} - null if the entity was updated otherwise (e.g. id does not exist) returns the book.
     * @throws IllegalArgumentException if the given entity is null.
     */
    @Override
    public Optional<T> update(T entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        validator.validate(entity);
        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k, v) -> entity));
    }
}
