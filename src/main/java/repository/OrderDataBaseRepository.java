package repository;


import model.Order;
import model.validators.IValidator;
import org.postgresql.util.PSQLException;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.*;
import java.util.*;


public class OrderDataBaseRepository extends InMemoryRepository<Integer, Order> {
    private static final String URL = "jdbc:postgresql://localhost:5432/bookstore";
    private static final String USER = System.getProperty("user");
    private static final String PASSWORD = System.getProperty("password");

    /**
     * Creates a repository for a specific type implementation object.
     *
     * @param validator
     */
    public OrderDataBaseRepository(IValidator<Order> validator) {
        super(validator);
        List<Order> orders = this.findAll();

        orders.forEach(order -> {
            try {
                super.save(order);
            } catch (ValidatorException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (org.xml.sax.SAXException e) {
                e.printStackTrace();
            }
        });
    }

    public List<Order> findAll() {
        List<Order> orders = new ArrayList<>();

        String sql = "SELECT * FROM orders";

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Integer id = rs.getInt("id");
                Integer clientId = rs.getInt("clientid");
                Integer bookId = rs.getInt("bookid");
                Integer units = rs.getInt("units");
                double totalPrice = rs.getDouble("totalprice");

                Order order = new Order(id, clientId, bookId, units, totalPrice);
                orders.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }


    @Override
    public Optional<Order> save(Order entity) throws ValidatorException, ParserConfigurationException, IOException, SAXException {

        Optional<Order> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToDataBase(entity);
        return Optional.empty();
    }

    public void saveToDataBase(Order bookInventory) {

        String sql = "INSERT INTO orders (id, clientid, bookid, units, totalprice) VALUES (?,?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, bookInventory.getId());
            ps.setInt(2, bookInventory.getClientId());
            ps.setInt(3, bookInventory.getBookid());
            ps.setInt(4, bookInventory.getUnits());
            ps.setDouble(5, bookInventory.getTotalPrice());

            ps.executeUpdate();
        } catch (PSQLException e) {
            e.printStackTrace();
            //   System.out.println("The book already exists in the inventory");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Order> update(Order order) {
        Optional<Order> optional = null;
        try {
            optional = super.update(order);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (optional.isPresent()) {
            updateToDataBase(order);
            return optional;
        }

        return Optional.empty();
    }

    private void updateToDataBase(Order order) {
        String sql = "UPDATE orders SET clientid=?, bookid=?, units=?, totalprice=? WHERE id=?";

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, order.getClientId());
            ps.setInt(2, order.getBookid());
            ps.setInt(3, order.getUnits());
            ps.setDouble(4, order.getTotalPrice());

            ps.setInt(5, order.getId());


            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Order> delete(Integer id) {

        Optional<Order> optional = super.delete(id);

        if (optional.isPresent())
            deleteFromDatabase(id);

        return optional;
    }

    public void deleteFromDatabase(Integer id) {

        String sql = "DELETE FROM orders WHERE id = ?";

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
