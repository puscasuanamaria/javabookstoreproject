package repository;

import model.Book;
import model.Client;
import model.validators.IValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class ClientXMLRepository extends InMemoryRepository<Integer, Client> {
    private String fileName;

    public ClientXMLRepository(IValidator<Client> validator, String fileName) throws Exception {
        super(validator);
        this.fileName = fileName;
        loadClientsFromXML();
    }

    private void loadClientsFromXML() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document clientsDoc = docBuilder.parse(fileName);

            Element root = clientsDoc.getDocumentElement();
            NodeList clientNodes = root.getChildNodes();

            for (int i = 0; i < clientNodes.getLength(); i++) {
                Node currentClientNodes = clientNodes.item(i);
                if (currentClientNodes instanceof Element) {
                    Client currentClient = createClientFromNode(currentClientNodes);
                    super.save(currentClient);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Client createClientFromNode(Node currentClientNode) {

        Element clientElement = (Element) currentClientNode;

        Client client = new Client();

        String id = clientElement.getAttribute("id");
        client.setId(Integer.parseInt(id));

        String name = getTextFromTagName(clientElement, "name");
        client.setName(name);

        String dateOfRegistration = getTextFromTagName(clientElement, "dateOfRegistration");
        client.setDateOfRegistration(dateOfRegistration);

        return client;
    }

    private String getTextFromTagName(Element clientElement, String tagName) {
        NodeList children = clientElement.getElementsByTagName(tagName);
        Node valueNode = children.item(0);

        return valueNode.getTextContent();
    }

    @Override
    public Optional<Client> save(Client client) {
        Optional<Client> optional = null;
        try {
            optional = super.save(client);
        } catch (ValidatorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        if (optional.isPresent()) {
            return optional;
        }
        saveToXML(client);
        return Optional.empty();
    }

    private void saveToXML(Client client) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document clientsDoc = docBuilder.parse(fileName);

            addClientToDOM(clientsDoc, client);

            transformSourceTreeIntoResultTree(clientsDoc);

        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }

    private void transformSourceTreeIntoResultTree(Document document) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();

            transformer.transform(new DOMSource(document),
                    new StreamResult(new File(fileName)));

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private void addClientToDOM(Document clientsDoc, Client client) {
        Node root = clientsDoc.getDocumentElement();

        Element clientElement = createClientElement(clientsDoc, client);

        root.appendChild(clientElement);
    }

    private Element createClientElement(Document clientsDoc, Client client) {
        Element clientElement = clientsDoc.createElement("client");

        clientElement.setAttribute("id", String.valueOf(client.getId()));
        addChildWithTextContent(clientsDoc, clientElement, "name", client.getName());
        addChildWithTextContent(clientsDoc, clientElement, "dateOfRegistration", client.getDateOfRegistration());

        return clientElement;
    }

    private void addChildWithTextContent(Document clientsDoc, Element clientElement, String tagName, String textContent) {
        Element element = clientsDoc.createElement(tagName);
        element.setTextContent(textContent);
        clientElement.appendChild(element);
    }

    @Override
    public Optional<Client> delete(Integer id) {
        Optional<Client> optional = super.delete(id);

        if (optional.isPresent())
            deleteNodeAfterId(id);

        return optional;
    }

    private void deleteNodeAfterId(Integer id) {
        try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document clientsDoc = docBuilder.parse(fileName);

            Element root = clientsDoc.getDocumentElement();
            NodeList clientsNodes = root.getChildNodes();

            for (int i = 0; i < clientsNodes.getLength(); i++) {
                Node clientNode = clientsNodes.item(i);
                if (clientNode instanceof Element) {
                    Node attributeFirst = clientNode.getAttributes().item(0);
                    if (attributeFirst != null && Integer.parseInt(attributeFirst.getTextContent()) == id) {
                        clientNode.getParentNode().removeChild(clientNode);
                        i = clientsNodes.getLength();
                    }
                }
            }

            transformSourceTreeIntoResultTree(clientsDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Client> update(Client client) {
        Optional<Client> optional = null;
        try {
            optional = super.update(client);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (optional.isPresent()) {
            return optional;
        }
        updateToXMLFile(client);
        return Optional.empty();
    }

    private void updateToXMLFile(Client client) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document clientsDoc = docBuilder.parse(fileName);

            Element root = clientsDoc.getDocumentElement();
            NodeList clientNodes = root.getChildNodes();


            for (int i = 0; i < clientNodes.getLength(); i++) {
                Node clientNode = clientNodes.item(i);
                if (clientNode instanceof Element) {
                    Node attribute = clientNode.getAttributes().item(0);
                    Integer idOld = Integer.parseInt(attribute.getTextContent());
                    Integer idNew = client.getId();
                    if (idOld.equals(idNew)) {
                        root.replaceChild(createClientElement(clientsDoc, client), clientNode);
                    }
                }
            }

            transformSourceTreeIntoResultTree(clientsDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
