package repository;
import model.Book;
import model.Inventory;
import model.validators.IValidator;
import org.postgresql.util.PSQLException;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InventoryDatabaseRepository extends InMemoryRepository<Integer, Inventory>{
    private static final String URL = "jdbc:postgresql://localhost:5432/bookstore";
    private static final String USER = System.getProperty("user");
    private static final String PASSWORD = System.getProperty("password");


    /**
     * Creates a repository for a specific type implementation object.
     *
     * @param validator
     */
    public InventoryDatabaseRepository(IValidator<Inventory> validator) {
        super(validator);
        List<Inventory> booksInvetory = this.findAll();

        booksInvetory.forEach(book -> {
            try {
                super.save(book);
            } catch (ValidatorException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        });
    }

    public List<Inventory> findAll() {
        List<Inventory> booksInventory = new ArrayList<>();

        String sql = "SELECT * FROM inventories";

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                Integer bookId = rs.getInt("bookid");
                Integer units = rs.getInt("units");

                Inventory bookInventory = new Inventory(bookId, units);
                booksInventory.add(bookInventory);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return booksInventory;
    }

    @Override
    public Optional<Inventory> save(Inventory entity) throws ValidatorException, ParserConfigurationException, SAXException, IOException {
        Optional<Inventory> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToDataBase(entity);
        return Optional.empty();
    }

    public void saveToDataBase(Inventory bookInventory) {

        String sql = "INSERT INTO inventories (bookid, units) VALUES (?, ?)";

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, bookInventory.getId());
            ps.setInt(2, bookInventory.getUnits());

            ps.executeUpdate();
        } catch (PSQLException e) {
            e.printStackTrace();
         //   System.out.println("The book already exists in the inventory");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public Optional<Book> delete(Integer id) {
//
//        Optional<Book> optional = super.delete(id);
//
//        if (optional.isPresent())
//            deleteFromDatabase(id);
//
//        return optional;
//    }
//
//    public void deleteFromDatabase(Integer id) {
//
//        String sql = "DELETE FROM book WHERE id = ?";
//
//        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
//             PreparedStatement ps = connection.prepareStatement(sql)) {
//
//            ps.setLong(1, id);
//            ps.executeUpdate();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
    @Override
    public Optional<Inventory> update(Inventory inventory){
        Optional<Inventory> optional = null;
        try {
            optional = super.update(inventory);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (optional.isPresent()) {
            updateToDataBase(inventory);
            return optional;
        }

        return Optional.empty();
    }

    private void updateToDataBase(Inventory inventory) {
        String sql = "UPDATE inventories SET units=? WHERE bookid= ?";

        try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, inventory.getUnits());
            ps.setInt(2, inventory.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

