package repository;

import model.Book;
import model.validators.IValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Optional;


public class BookXMLRepository extends InMemoryRepository<Integer, Book> {
    private String fileName;

    public BookXMLRepository(IValidator<Book> validator, String fileName) throws Exception {
        super(validator);
        this.fileName = fileName;
        loadBooksFromXML();
    }

    private void loadBooksFromXML() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document booksDoc = docBuilder.parse("./data/books.xml");

            Element root = booksDoc.getDocumentElement();
            NodeList bookNodes = root.getChildNodes();

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentBookNode = bookNodes.item(i);
                if (currentBookNode instanceof Element) {
                    Book currentBook = createBookFromNode(currentBookNode);
                    super.save(currentBook);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Book createBookFromNode(Node currentBookNode) {

        Element bookElement = (Element) currentBookNode;

        Book book = new Book();

        String id = bookElement.getAttribute("id");
        book.setId(Integer.parseInt(id));

        String name = getTextFromTagName(bookElement, "name");
        book.setName(name);

        String author = getTextFromTagName(bookElement, "author");
        book.setAuthor(author);

        String publisher = getTextFromTagName(bookElement, "publisher");
        book.setPublisher(publisher);

        String yearOfPublication = getTextFromTagName(bookElement, "yearOfPublication");
        book.setYearOfPublication(yearOfPublication);

        double price = Double.parseDouble(getTextFromTagName(bookElement, "price"));
        book.setPrice(price);

        return book;
    }

    private String getTextFromTagName(Element bookElement, String tagName) {
        NodeList children = bookElement.getElementsByTagName(tagName);
        Node valueNode = children.item(0);

        return valueNode.getTextContent();
    }

    @Override
    public Optional<Book> save(Book book) throws IOException, ParserConfigurationException, SAXException, ValidatorException {
        Optional<Book> optional = super.save(book);
        if (optional.isPresent()) {
            return optional;
        }
        saveToXML(book);
        return Optional.empty();
    }

    private void saveToXML(Book book) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document booksDoc = docBuilder.parse("./data/books.xml");

            addBookToDOM(booksDoc, book);

            transformSourceTreeIntoResultTree(booksDoc);

        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }

    private void transformSourceTreeIntoResultTree(Document document) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();

            transformer.transform(new DOMSource(document),
                    new StreamResult(new File(fileName)));

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private void addBookToDOM(Document booksDoc, Book book) {
        Node root = booksDoc.getDocumentElement();

        Element bookElement = createBookElement(booksDoc, book);

        root.appendChild(bookElement);
    }

    private Element createBookElement(Document booksDoc, Book book) {
        Element bookElement = booksDoc.createElement("book");

        bookElement.setAttribute("id", String.valueOf(book.getId()));
        addChildWithTextContent(booksDoc, bookElement, "name", book.getName());
        addChildWithTextContent(booksDoc, bookElement, "author", book.getAuthor());
        addChildWithTextContent(booksDoc, bookElement, "publisher", book.getPublisher());
        addChildWithTextContent(booksDoc, bookElement, "yearOfPublication", book.getYearOfPublication());
        addChildWithTextContent(booksDoc, bookElement, "price",
                Double.toString(book.getPrice()));
        return bookElement;
    }

    private void addChildWithTextContent(Document booksDoc, Element bookElement, String tagName, String textContent) {
        Element element = booksDoc.createElement(tagName);
        element.setTextContent(textContent);
        bookElement.appendChild(element);
    }

    @Override
    public Optional<Book> delete(Integer id) {
        Optional<Book> optional = super.delete(id);

        if (optional.isPresent())
            deleteNodeAfterId(id);

        return optional;
    }

    private void deleteNodeAfterId(Integer id) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document booksDoc = docBuilder.parse(fileName);

            Element root = booksDoc.getDocumentElement();
            NodeList bookNodes = root.getChildNodes();

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node bookNode = bookNodes.item(i);
                if (bookNode instanceof Element) {
                    Node attributeFirst = bookNode.getAttributes().item(0);
                    if (attributeFirst != null && Integer.parseInt(attributeFirst.getTextContent()) == id) {
                        bookNode.getParentNode().removeChild(bookNode);
                        i = bookNodes.getLength();
                    }
                }
            }

            transformSourceTreeIntoResultTree(booksDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Book> update(Book book){
        Optional<Book> optional = null;
        try {
            optional = super.update(book);
        } catch (ValidatorException e) {
            e.printStackTrace();
        }

        if (optional.isPresent()) {
            return optional;
        }
        updateToXMLFile(book);
        return Optional.empty();
    }

    private void updateToXMLFile(Book book) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            Document booksDoc = docBuilder.parse(fileName);

            Element root = booksDoc.getDocumentElement();
            NodeList bookNodes = root.getChildNodes();


            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node bookNode = bookNodes.item(i);
                if (bookNode instanceof Element) {
                    Node attribute = bookNode.getAttributes().item(0);
                    Integer idOld = Integer.parseInt(attribute.getTextContent());
                    Integer idNew = book.getId();
                    if (idOld.equals( idNew)){
                        root.replaceChild(createBookElement(booksDoc, book), bookNode);
                    }
                }
            }

            transformSourceTreeIntoResultTree(booksDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
