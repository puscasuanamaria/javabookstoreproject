package repository;


import model.Book;
import model.Entity;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Optional;

/**
 * Interface for CRUD operations on a reposity for a specific type.
 *
 * @author radu - modified by ana.
 */

public interface IRepository<ID, T extends Entity<ID>> {
    /**
     * Find the entity with the given {@code id}.
     *
     * @param id must be not null.
     * @return a {@code Optional} encapsulating the entity with the given id.
     * @throws IllegalArgumentException if the given id is null.
     */
    Optional<T> findOne(ID id);

    /**
     * @return all entities.
     */
    Iterable<T> findAll();

    /**
     * Saves the given entity.
     *
     * @param entity must not be null.
     * @return a {@code Optional} - null if the entity was saved otherwise (e.g. id already exists) returns the entity.
     * @throws IllegalArgumentException if the given entity is null.
     * @throws ValidatorException if the entity is not valid.
     */
    Optional<T> save(T entity) throws ValidatorException, IOException, SAXException, ParserConfigurationException;

    /**
     * Removes the entity with the given id.
     *
     * @param id must not be null.
     * @return a {@code Optional} - null if there is no entity with the given id, otherwise the removed entity.
     * @throws IllegalArgumentException if the given id is null.
     */
    Optional<T> delete(ID id);

    /**
     * Updates the given entity.
     *
     * @param entity must not be null.
     * @return a {@code Optional} - null if the entity was updated otherwise (e.g. id does not exist) returns the
     *          entity.
     * @throws IllegalArgumentException if the given entity is null.
     * @throws ValidatorException if the entity is not valid.
     */
    Optional<T> update(T entity) throws ValidatorException;
}
