package service;

import model.Book;
import model.Inventory;
import model.Order;
import org.xml.sax.SAXException;
import repository.IRepository;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class OrderService {
    private IRepository<Integer, Order> orderRepository;
    private IRepository<Integer, Inventory> inventoryRepository;
    private IRepository<Integer, Book> bookRepository;


    public OrderService(IRepository<Integer, Order> orderRepository, IRepository<Integer, Inventory> inventoryRepository, IRepository<Integer, Book> bookRepository) {
        this.orderRepository = orderRepository;
        this.inventoryRepository = inventoryRepository;
        this.bookRepository = bookRepository;
    }

    /**
     * Saves the given Order in the repository.
     *
     * @param order must not be null.
     */
    public void addOrder(Order order) throws ValidatorException, ParserConfigurationException, SAXException, IOException {
        Optional<Inventory> optionalInventory = inventoryRepository.findOne(order.getBookid());
        if (optionalInventory.isPresent()) {
            Inventory newInventory = optionalInventory.get();

            if (newInventory.getUnits() - order.getUnits() >= 0) {

                Optional<Book> optionalBook = bookRepository.findOne(order.getBookid());
                if (optionalBook.isPresent()) {
                    Book newBook = optionalBook.get();
                    order.setTotalPrice(order.getUnits()*newBook.getPrice());
                }

                newInventory.setUnits(newInventory.getUnits() - order.getUnits());
                inventoryRepository.update(newInventory);

                orderRepository.save(order);
            } else {
                System.out.println("Book out of stock!");
            }
        }
    }

    /**
     * Returns all orders from repository.
     *
     * @return a set of {@code Orders}.
     */
    public Set<Order> getAllOrders() {

        Iterable<Order> orders = orderRepository.findAll();
        return StreamSupport.stream(orders.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Delete an order from repository by a given Id.
     *
     * @param id
     */
    public void deleteOrderById(int id){
        orderRepository.delete(id);
    }

    /**
     * Update an order from repository.
     *
     * @param order must not be null.
     * @throws ValidatorException
     */
    public void updateOrder (Order order) throws ValidatorException {
        this.orderRepository.update(order);
    }


    public void orderClientBySpendedMoney(){
        Iterable<Order> initialOrderList = orderRepository.findAll();

        Stream<Order> stream1 = getStreamFromIterable(initialOrderList);
        Stream<Order> stream2 = getStreamFromIterable(initialOrderList);

        Map<Integer, Double> totalNoThings = stream1
                .collect(Collectors.groupingBy(Order::getClientId,
                        Collectors.summingDouble(Order::getTotalPrice)));

        List<Order> sorted = stream2
                .sorted(
                Comparator.comparing((Order x)->totalNoThings.get(x.getClientId()))
                        .thenComparing(Order::getTotalPrice).reversed())
                .collect(Collectors.toList());

        sorted
                .forEach(System.out::println);
    }

    public static <T> Stream<T>
    getStreamFromIterable(Iterable<T> iterable)
    {

        // Convert the Iterable to Spliterator
        Spliterator<T>
                spliterator = iterable.spliterator();

        // Get a Sequential Stream from spliterator
        return StreamSupport.stream(spliterator, false);
    }

}
