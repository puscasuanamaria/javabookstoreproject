package service;

import model.Inventory;
import org.postgresql.util.PSQLException;
import org.xml.sax.SAXException;
import repository.IRepository;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class InventoryService {
    private IRepository<Integer, Inventory> repository;

    public InventoryService(IRepository<Integer, Inventory> repository) {
        this.repository = repository;
    }

    /**
     * Saves the given inventory in the repository.
     *
     * @param inventory must not be null.
     */
    public void addInventory(Inventory inventory) {
        try {
            repository.save(inventory);
        }catch (ValidatorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns all books from repository.
     *
     * @return a set of {@code Books}.
     */
    public Set<Inventory> getAllInventoryBooks() {

        Iterable<Inventory> booksInventory = repository.findAll();
        return StreamSupport.stream(booksInventory.spliterator(), false).collect(Collectors.toSet());
    }

//    /**
//     * Delete a book from repository by a given Id.
//     *
//     * @param id
//     */
//    public void deleteBookById(int id){
//        repository.delete(id);
//    }
//
//    /**
//     * Update a book from repository.
//     *
//     * @param book must not be null.
//     * @throws ValidatorException
//     */
//    public void updateBook (Book book) throws ValidatorException {
//        this.repository.update(book);
//    }
}
