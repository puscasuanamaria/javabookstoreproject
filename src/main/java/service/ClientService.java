package service;

import model.Book;
import model.Client;
import org.xml.sax.SAXException;
import repository.IRepository;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ClientService {

    private IRepository<Integer, Client> repository;

    public ClientService(IRepository<Integer, Client> repository) {
        this.repository = repository;
    }

    /**
     * Saves the given Client in the repository.
     *
     * @param client must not be null.
     */
    public void addClient(Client client) throws ValidatorException, ParserConfigurationException, SAXException, IOException {
        repository.save(client);
    }

    /**
     * Returns all clients from repository.
     *
     * @return a set of {@code Clients}.
     */
    public Set<Client> getAllClients() {
        Iterable<Client> clients = repository.findAll();
        return StreamSupport.stream(clients.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Delete a client from repository.
     *
     * @param id for a client.
     */
     public void deleteClientById(int id){
        repository.delete(id);
    }

    /**
     * Update a client from repository.
     *
     * @param client must not be null.
     * @throws ValidatorException
     */
    public void updateClient (Client client) throws ValidatorException {
        this.repository.update(client);
    }

    public void getClientsByName(String name) {
        Iterable<Client> initialClientList = repository.findAll();

        Stream<Client> stream = getStreamFromIterable(initialClientList);

        List<Client> clientsByName = stream
                .filter(e -> e.getName().contains(name))
                .collect(Collectors.toList());
        clientsByName
                .forEach(System.out::println);
    }

    public static <T> Stream<T>
    getStreamFromIterable(Iterable<T> iterable)
    {

        // Convert the Iterable to Spliterator
        Spliterator<T>
                spliterator = iterable.spliterator();

        // Get a Sequential Stream from spliterator
        return StreamSupport.stream(spliterator, false);
    }

}
