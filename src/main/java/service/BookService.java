package service;

import model.Book;
import org.xml.sax.SAXException;
import repository.IRepository;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.Math.min;
import static java.util.stream.Collectors.toMap;

public class BookService {
    private IRepository<Integer, Book> repository;

    public BookService(IRepository<Integer, Book> repository) {
        this.repository = repository;
    }

    /**
     * Saves the given Book in the repository.
     *
     * @param book must not be null.
     */
    public void addBook(Book book) throws ValidatorException, ParserConfigurationException, SAXException, IOException {
        repository.save(book);
    }

    /**
     * Returns all books from repository.
     *
     * @return a set of {@code Books}.
     */
    public Set <Book> getAllBooks() {

        Iterable<Book> books = repository.findAll();
        return StreamSupport.stream(books.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Delete a book from repository by a given Id.
     *
     * @param id
     */
    public void deleteBookById(int id){
        repository.delete(id);
    }

    /**
     * Update a book from repository.
     *
     * @param book must not be null.
     * @throws ValidatorException
     */
    public void updateBook (Book book) throws ValidatorException {
        this.repository.update(book);
    }

    public void groupBooksByAuthor() {
        Iterable<Book> initialBookList = repository.findAll();

        Stream<Book> stream = getStreamFromIterable(initialBookList);

        Map<String, List<Book>> booksByAuthor = stream
                .collect(Collectors.groupingBy(p -> p.getAuthor()));
        booksByAuthor
                .forEach((author, p) -> System.out.format("Author: %s: %s\n", author, p));
    }

    public void getBooksByPublisher(String publisher) {
        Iterable<Book> initialBookList = repository.findAll();

        Stream<Book> stream = getStreamFromIterable(initialBookList);

        List<Book> booksByPublisher = stream
                .filter(e -> e.publisher.contains(publisher))
                .collect(Collectors.toList());
        booksByPublisher
                .forEach(System.out::println);
    }

    /**
     * Split a list of books in two lists - by a given year of publication.
     * Create two lists of books - first one - with the year of publication smaller than the given year.
     *                           - second one - with the year of publication greater than the given year.
     *
     * @param year of publication - split element.
     * @return a list of books ordered by year - the result of the concatenation.
     */
    //paritioningby colectors
    public List<Book> getBooksByGivenYear(Integer year) {
        Iterable<Book> initialBookList = repository.findAll();

        Stream<Book> stream1 = getStreamFromIterable(initialBookList);
        Stream<Book> stream2 = getStreamFromIterable(initialBookList);

        List<Book> booksLessThanGivenYear = stream1
                .filter(book -> Integer.parseInt(book.getYearOfPublication()) <= year)
                .collect(Collectors.toList());

        List<Book> booksGreaterThanGivenYear = stream2
                .filter(book -> Integer.parseInt(book.getYearOfPublication()) > year)
                .collect(Collectors.toList());

        List<Book> bookList = Stream.concat(booksLessThanGivenYear.stream(), booksGreaterThanGivenYear.stream())
                .collect(Collectors.toList());

        return bookList;
    }

    /**
     * Split a list of books in two lists - by a given year of publication.
     * Create two lists of books - first one - with the year of publication smaller than the given year - ordered by title.
     *                           - second one - with the year of publication greater than the given year - ordered reversed by year of publication.
     *
     * @param year of publication - split element.
     * @return a list of books ordered by year - the result of the concatenation.
     */
    public List<Book> getOrderedBooksByGivenYear(Integer year) {
        Iterable<Book> initialBookList = repository.findAll();

        Stream<Book> stream1 = getStreamFromIterable(initialBookList);
        Stream<Book> stream2 = getStreamFromIterable(initialBookList);

        Stream<Book> booksLessThanGivenYear = stream1
                .filter(book -> Integer.parseInt(book.getYearOfPublication()) <= year)
                .sorted(Comparator.comparing(Book::getName));
//                .collect(Collectors.toList());

        Stream<Book> booksGreaterThanGivenYear = stream2
                .filter(book -> Integer.parseInt(book.getYearOfPublication()) > year)
                .sorted(Comparator.comparing(Book::getYearOfPublication)
                .reversed());
               // .collect(Collectors.toList());

        List<Book> bookList = Stream.concat(booksLessThanGivenYear, booksGreaterThanGivenYear)
                .collect(Collectors.toList());

        return bookList;
    }

    /**
     * Convert the Iterable to Spliterator.
     *
     * @param iterable
     * @param <T>
     * @return a Sequential Stream from spliterator.
     */
    public static <T> Stream<T> getStreamFromIterable(Iterable<T> iterable)
    {

        Spliterator<T>
                spliterator = iterable.spliterator();

        return StreamSupport.stream(spliterator, false);
    }


    /**
     * Convert a Set into a List.
     *
     * @param sourceSet
     * @return a list
     */
    public List<Book> setToList(Set <Book> sourceSet) {
        List<Book> targetList = new ArrayList<Book>(sourceSet);
        return targetList;
    }

    /**
     * Calculate the numbers of keys needed in the map.
     * @param books
     * @param pageSize
     * @return a Stream that creates the sequence 0, pageSize, 2* pageSize,...
     */
    public Map<Integer, List<Book>> getNetxtBooks(Set<Book> books, int pageSize) {
        List<Book> booksList = setToList(books);
        return IntStream.iterate(0, i -> i + pageSize)
                .limit((booksList.size() + pageSize - 1) / pageSize)
                .boxed()
                .collect(toMap(i -> i / pageSize,
                        i -> booksList.subList(i, min(i + pageSize, booksList.size()))));
    }
}
