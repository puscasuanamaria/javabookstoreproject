package ui;

import model.Book;
import model.Client;
import model.Inventory;
import model.Order;
import org.xml.sax.SAXException;
import service.BookService;
import service.ClientService;
import service.InventoryService;
import service.OrderService;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Console {
    private BookService bookService;
    private ClientService clientService;
    private OrderService orderService;
    private InventoryService inventoryService;

    public Console(BookService bookService, ClientService clientService, OrderService orderService, InventoryService inventoryService) {
        this.bookService = bookService;
        this.clientService = clientService;
        this.orderService = orderService;
        this.inventoryService = inventoryService;
    }

    private void showMenu() {
        System.out.println("1. Book CRUD");
        System.out.println("2. Client CRUD");
        System.out.println("3. Inventory CRUD");
        System.out.println("4. Order CRUD");
        System.out.println("5. Raports");

        System.out.println("x. Exit");
    }

    /**
     * Show options for Book CRUD
     *
     * @throws IOException
     */
    public void runConsole() throws IOException {
        while (true) {
            showMenu();

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = bufferRead.readLine();
            switch (option) {
                case "1":
                    runBookCrud();
                    break;
                case "2":
                    runClientCrud();
                    break;
                case "3":
                    runInventoryCrud();
                    break;
                case "4":
                    runOrderCrud();
                    break;
                case "5":
                    runRaports();
                    break;
                case "x":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void runRaports() {
        while (true) {
            System.out.println();
            System.out.println("1. Get books by publisher");
            System.out.println("2. Books grouped by author");
            System.out.println("3. Sort clients based on the spent amount of money");
            System.out.println("4. Get clients by name");
            System.out.println("5. Get books by year");
            System.out.println("6. Get ordered books by year");

            System.out.println("7. Back");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = null;
            try {
                option = bufferRead.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (option) {
                case "1":
                    filtredByPublisher();
                    break;
                case "2":
                    groupByAuthor();
                    break;
                case "3":
                    sortOrdersByTotalSum();
                    break;
                case "4":
                    filterClientsbyName();
                    break;
                case "5":
                    getBooksByYear();
                    break;
                case "6":
                    getOrderedBooksByYear();
                    break;
                case "7":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void getOrderedBooksByYear() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter year: ");
            int year = Integer.parseInt(bufferRead.readLine());

            List<Book> list = bookService.getOrderedBooksByGivenYear(year);

            list.forEach(System.out::println);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void getBooksByYear() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter year: ");
            int year = Integer.parseInt(bufferRead.readLine());

            List<Book> list = bookService.getBooksByGivenYear(year);

            list.forEach(System.out::println);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void filterClientsbyName() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter name: ");
            String name = bufferRead.readLine();

            clientService.getClientsByName(name);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void sortOrdersByTotalSum() {
        orderService.orderClientBySpendedMoney();
    }

    private void groupByAuthor() {
        bookService.groupBooksByAuthor();
    }

    private void filtredByPublisher() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter publisher: ");
            String publisher = bufferRead.readLine();

            bookService.getBooksByPublisher(publisher);

        } catch (IOException ex) {
            ex.printStackTrace();

        }
    }

    private void runOrderCrud() {
        while (true) {
            System.out.println();
            System.out.println("1. Add an order");
            System.out.println("2. View all orders");

            System.out.println("3. Back");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = null;
            try {
                option = bufferRead.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (option) {
                case "1":
                    addOrders();
                    break;
                case "2":
                    printAllOrders();
                    break;
                case "3":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void printAllOrders() {
        Set<Order> orders = orderService.getAllOrders();
        orders.stream().forEach(System.out::println);
    }

    private void addOrders() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter id: ");
            int id = Integer.parseInt(bufferRead.readLine());
            System.out.println("Enter client id: ");
            int clientID = Integer.parseInt(bufferRead.readLine());
            System.out.println("Enter book id: ");
            int bookID = Integer.parseInt(bufferRead.readLine());
            System.out.println("Enter units: ");
            int units = Integer.parseInt(bufferRead.readLine());

            Order order = new Order(id, clientID, bookID, units);
            order.setId(id);

            orderService.addOrder(order);

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ValidatorException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        System.out.println("The order was added!");
    }

    private void runInventoryCrud() {
        while (true) {
            System.out.println();
            System.out.println("1. Add a  book in inventory");
            System.out.println("2. View all books from inventory");
            System.out.println("3. Back");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = null;
            try {
                option = bufferRead.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (option) {
                case "1":
                    addBooksInInventory();
                    break;
                case "2":
                    printAllBooksFromInventory();
                    break;
                case "3":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void printAllBooksFromInventory() {
        Set<Inventory> inventories = inventoryService.getAllInventoryBooks();
        inventories.stream().forEach(System.out::println);
    }

    private void addBooksInInventory() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter id: ");
            int id = Integer.parseInt(bufferRead.readLine());
            System.out.println("Enter units: ");
            int units = Integer.parseInt(bufferRead.readLine());

            Inventory inventory = new Inventory(id, units);
            inventory.setId(id);

            inventoryService.addInventory(inventory);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("The book was added!");
    }

    private void runBookCrud() throws IOException {
        while (true) {
            System.out.println();
            System.out.println("1. Add a book");
            System.out.println("2. View all books");
            System.out.println("3. Delete a book by a given id");
            System.out.println("4. Update a book");
            System.out.println("5. View all books with paging");
            System.out.println("6. Back");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = bufferRead.readLine();
            switch (option) {
                case "1":
                    addBooks();
                    break;
                case "2":
                    printAllBooks();
                    break;
                case "3":
                    deleteBookById();
                    break;
                case "4":
                    updateBook();
                    break;
                case "5":
                    getNextBooksPaging();
                    break;
                case "6":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void getNextBooksPaging() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        int pageSize = 0;

        try {
            System.out.println("Enter page size: ");
            pageSize = Integer.parseInt(bufferRead.readLine());

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Set<Book> books = bookService.getAllBooks();
        Map<Integer, List<Book>> nextBooks = bookService.getNetxtBooks(books, pageSize);

        System.out.println("Number of pages: " + nextBooks.size());

        System.out.println("enter 'n' - for next; 'x' - for exit: ");

        int currentPage = 0;

        while (true) {
            String cmd = null;

            try {
                cmd = bufferRead.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (cmd.equals("x")) {
                System.out.println("exit");
                break;
            }
            if (!cmd.equals("n")) {
                System.out.println("this option is not yet implemented");
                continue;
            }

            if (currentPage == nextBooks.size()) {
                System.out.println("no more books");
                break;
            }

            for (Book book : nextBooks.get(currentPage))
                System.out.println(book);

            currentPage++;
        }
    }

    private void updateBook() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter id: ");
            int id = Integer.parseInt(bufferRead.readLine());
            System.out.println("Enter title: ");
            String name = bufferRead.readLine();
            System.out.println("Enter author: ");
            String author = bufferRead.readLine();
            System.out.println("Enter publisher: ");
            String publisher = bufferRead.readLine();
            System.out.println("Enter year of publication: ");
            String yearOfPublication = bufferRead.readLine();
            System.out.println("Enter price: ");
            double price = Double.parseDouble(bufferRead.readLine());

            Book book = new Book(id, name, author, publisher, yearOfPublication, price);
            book.setId(id);

            bookService.updateBook(book);

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
        System.out.println("The book was updated!");
    }

    private void deleteBookById() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter id: ");
            int id = Integer.parseInt(bufferRead.readLine());

            bookService.deleteBookById(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("The book was deleted!");
    }

    private void addBooks() throws IOException {

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter id: ");
            int id = Integer.parseInt(bufferRead.readLine());
            System.out.println("Enter title: ");
            String name = bufferRead.readLine();
            System.out.println("Enter author: ");
            String author = bufferRead.readLine();
            System.out.println("Enter publisher: ");
            String publisher = bufferRead.readLine();
            System.out.println("Enter year of publication: ");
            String yearOfPublication = bufferRead.readLine();
            System.out.println("Enter price: ");
            double price = Double.parseDouble(bufferRead.readLine());

            Book book = new Book(id, name, author, publisher, yearOfPublication, price);
            book.setId(id);

            bookService.addBook(book);

        } catch (IOException | ValidatorException ex) {
            ex.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        System.out.println("The book was added!");
    }

    private void printAllBooks() {
        Set<Book> books = bookService.getAllBooks();
        books.stream().forEach(System.out::println);
    }

    private void runClientCrud() throws IOException {
        while (true) {
            System.out.println("1. Add a client");
            System.out.println("2. View all clients");
            System.out.println("3. Delete a client by given id");
            System.out.println("4. Update a client");
            System.out.println("5. Back");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = bufferRead.readLine();
            switch (option) {
                case "1":
                    addClients();
                    break;
                case "2":
                    printAllCllients();
                    break;
                case "3":
                    deleteClientById();
                    break;
                case "4":
                    updateClient();
                    break;
                case "5":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void updateClient() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.println("Enter id: ");
            int id = Integer.parseInt(bufferRead.readLine());
            System.out.println("Enter name: ");
            String name = bufferRead.readLine();
            System.out.println("Enter date of registration: ");
            String dateOfRegistration = bufferRead.readLine();

            Client client = new Client(id, name, dateOfRegistration);
            client.setId(id);

            clientService.updateClient(client);

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ValidatorException e) {
            e.printStackTrace();
        }
        System.out.println("The client was updated!");
    }

    private void deleteClientById() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter id: ");
            int id = Integer.parseInt(bufferRead.readLine());

            clientService.deleteClientById(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("The client was deleted!");
    }

    private void addClients() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Enter id: ");
            int id = Integer.parseInt(bufferRead.readLine());
            System.out.println("Enter name: ");
            String name = bufferRead.readLine();
            System.out.println("Enter date of registration: ");
            String dateOfRegistration = bufferRead.readLine();

            Client client = new Client(id, name, dateOfRegistration);
            client.setId(id);

            clientService.addClient(client);

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ValidatorException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }
        System.out.println("The client was added!");
    }

    private void printAllCllients() {
        Set<Client> clients = clientService.getAllClients();
        clients.stream().forEach(System.out::println);
    }
}
