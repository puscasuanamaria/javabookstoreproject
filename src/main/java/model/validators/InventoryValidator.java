package model.validators;

import model.Inventory;

public class InventoryValidator implements IValidator<Inventory> {
    @Override
    public void validate(Inventory bookInventory) throws ValidatorException {
        if (bookInventory.getId() <= 0) {
            throw new BookStoreException("This is an invalid id number");
        }

        if (bookInventory.getUnits() <= 0) {
            throw new BookStoreException("This is an invalid unit number");
        }
    }
}
