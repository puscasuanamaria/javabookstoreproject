package model.validators;

import model.Order;

public class OrderValidator implements IValidator<Order> {
    @Override
    public void validate(Order order) throws ValidatorException {
        if (order.getClientId() <= 0) {
            throw new BookStoreException("This is an invalid id number");
        }

        if (order.getId() <= 0) {
            throw new BookStoreException("This is an invalid id number");
        }
    }
}
