package model.validators;

import sun.security.validator.ValidatorException;

public interface IValidator <T>{

    void validate(T entity) throws ValidatorException;
}
