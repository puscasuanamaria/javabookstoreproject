package model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Order extends Entity<Integer> {
    private Integer clientId;
    private Integer bookId;
    private Integer units;
    private Double totalPrice;

    public Order(Integer integer, Integer clientId, Integer bookId, Integer units, Double totalPrice) {
        super(integer);
        this.clientId = clientId;
        this.bookId = bookId;
        this.units = units;
        this.totalPrice = totalPrice;
    }

    public Order(Integer clientId, Integer bookid, Integer units, Double totalPrice) {
        this.clientId = clientId;
        this.bookId = bookid;
        this.units = units;
        this.totalPrice = totalPrice;
    }

    public Order(Integer integer,Integer clientId, Integer bookid, Integer units) {
        super(integer);
        this.clientId = clientId;
        this.bookId = bookid;
        this.units = units;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getBookid() {
        return bookId;
    }

    public void setBookid(Integer bookid) {
        this.bookId = bookid;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + getId() +
                "clientId=" + clientId +
                ", bookId=" + bookId +
                ", units=" + units +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
