package model;

public class Inventory extends Entity < Integer > {
    private int units;

    public Inventory(Integer bookId, int units) {
        super(bookId);
        this.units = units;
    }

    public Inventory(int units) {
        this.units = units;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "bookId=" + getId() +
                ", units=" + units +
                '}';
    }
}
