import model.Book;
import model.Client;
import model.Inventory;
import model.Order;
import model.validators.*;
import repository.*;
import service.BookService;
import service.ClientService;
import service.InventoryService;
import service.OrderService;
import ui.Console;

import java.io.File;
import java.io.IOException;
import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) throws Exception {
//        //in-memory repo
//        IValidator<Book> bookValidator = new BookValidator();
//        IRepository<Integer, Book> bookRepository = new InMemoryRepository<>(bookValidator);
//        BookService bookService = new BookService(bookRepository);
//        Console console = new Console(bookService);
//        console.runConsole();

        //file repo
        try {
            System.out.println(new File("./data/").getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * In fileXML book
         //         */
//        IValidator<Book> bookValidator = new BookValidator();
//        IRepository<Integer, Book> bookXMLRepository = new BookXMLRepository(bookValidator, "./data/books.xml");
//        BookService bookService = new BookService(bookXMLRepository);

        /**
         * In fileXML client
//         */
//        IValidator<Client> clientValidator = new ClientValidator();
//        IRepository<Integer, Client> clientXMLRepository = new ClientXMLRepository(clientValidator, "./data/clients.xml");
//        ClientService clientService = new ClientService(clientXMLRepository);
//
//        Console console = new Console(bookService, clientService);
//        console.runConsole();


        IValidator<Book> bookValidator = new BookValidator();
        IRepository<Integer, Book> bookDatabaseRepository = new BookDatabaseRepository(bookValidator);
        BookService bookService = new BookService(bookDatabaseRepository);

        IValidator<Client> clientValidator = new ClientValidator();
        IRepository<Integer, Client> clientDatabaseRepository = new ClientDatabaseRepository(clientValidator);
        ClientService clientService = new ClientService(clientDatabaseRepository);

        IValidator<Inventory> inventoryIValidator = new InventoryValidator();
        IRepository<Integer, Inventory> inventoryDatabaseRepository = new InventoryDatabaseRepository(inventoryIValidator);
        InventoryService inventoryService = new InventoryService(inventoryDatabaseRepository);

        IValidator<Order> orderValidator = new OrderValidator();
        IRepository<Integer, Order> orderDatabaseRepository = new OrderDataBaseRepository(orderValidator);
        OrderService orderService = new OrderService(orderDatabaseRepository, inventoryDatabaseRepository, bookDatabaseRepository);

        Console console = new Console(bookService, clientService, orderService, inventoryService);
        console.runConsole();

        System.out.print("Sa mearga!");

    }
}
